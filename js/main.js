/* 
    Поясніть своїми словами, як ви розумієте поняття асинхронності у Javascript
        - JS - синхронна мова програмування, скрипт виконується строка за строкою, але 
                є можливість виконання деяких операцій без очікування завершення попередніх операцій.
                Одна з можливостей це реалізувати - за допомогою promise та async/await
*/


    const container = document.querySelector('.container');
    const buttonSearch = document.querySelector('.button-ip');
    const preloader = document.querySelector('.preloader-container');

    const searchUser = async () => {
    try {
        preloader.style.display = 'block';
        const user = await fetch("http://ip-api.com/json/?fields=61439").then(data => data.json());
        const data = await fetch(`http://ip-api.com/json/${user.query}`).then(data => data.json());
        console.log(data);

        const {timezone, country, regionName, city} = data;

        const ol = document.createElement('ol');

        ol.classList.add('list-info');

        ol.innerHTML = `
            <li>Континент: <span>${timezone}</span></li>
            <li>Країна: <span>${country}</span></li>
            <li>Регіон: <span>${regionName}</span></li>
            <li>Місто: <span>${city}</span></li>
        `;

        container.appendChild(ol);

        preloader.style.display = 'none';
        
    } catch(err) {
        console.error('Помилка:', err);

        preloader.style.display = 'none';
    }
}

buttonSearch.addEventListener('click', searchUser);